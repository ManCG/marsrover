# Mars rover

##Description

Develop an api that moves a rover around on a grid.

- You will have to provide the map size
- You will have to provide the number of obstacles and their position
- You will have to provide the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
- The rover receives a character array of commands.
- Implement commands that move the rover forward/backward (f,b).
- Implement commands that turn the rover left/right (l,r).
- Implement wrapping from one edge of the grid to another. (planets are spheres after all)
- Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point and reports the obstacle.

## How to
We want to see your changes but not make them public. In order to do so create a new repo on bitbucket based on this project, make all the changes and give us access to see them ;).

We need the commit list in order to check the evolution, naming and changes you made during the process.

## What we expect
Our intern have made as good as it could but not as we would. 

- Feel free to change as much code as you want, but you'll have to make it readable and maintainable.
- Use any JVM language.
- Feel free to use any pattern, framework or whatever you want to.
- Bug free will be a plus.

---

##Implementation details

The purpose of the structure of the project is aimed to be readable and extensible.
### Readable
- *Entities* are immutable: None of them need to be concerned with affecting others state's entities when user sends a new command.
i.e.:  When User sends a command with `UserConsoleCommand` it uses `PlanetModifier` to create a new `Planet`and return the new `Planet` to the user.
- Easier to reason about application state, since entities are immutable so the state is isolated. A new state of any Entity is only dependent on the input parameters.
- Encapsulated state makes easier to test *Entities* in isolation therefore tests have no implicit dependencies.
- No hidden dependencies: Constructor dependency injection of `PlanetModifier` when creating `UserConsoleCommand`.
- Avoidance of null as function parameters to remove extra checking in the callee.

### Extensible
- If parallelization needed, since *Entities* are immutable, there is no need to thread synchronization.
- If one or more Rovers will be required, `UserConsoleCommand` can be extended with a direction for each new Rover.
- If 3D is required, `Coordinate` can be extended using Z coordinate, translation logic in `UserConsoleCommand`.
- If a new command is required, it will placed in `Command` and the logic will be placed in `UserConsoleCommand`.
- If another way to translate commands is needed, as an input file, a new class could be placed in ports (package).
- If obstacle need to occupy more than one position it will not require to modify interface of objects that use it.

**Caveat**: In the main() loop, in each command creates a new `Map` object and will be referenced in the same variable, the old `Map` object will be unreachable and the Garbage Collector will free the occupied memory from the heap.
